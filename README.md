
# nginx-lb

nginx-lb practice workshop


## Prerequisites

 - [Install Docker](https://docs.docker.com/engine/install/ubuntu/)
 - [Enable Docker Service](https://docs.docker.com/engine/install/linux-postinstall/)
 - [Install NodeJS and npm](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-20-04)

## Install modules

Install docker-compose with apt

```bash
sudo apt install docker-compose
```

Install 'express' NPM module

```bash
npm install express
```

## Start an app

```bash
sudo docker-compose up --build --scale api=2
```